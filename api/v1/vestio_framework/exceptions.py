# DRF
from rest_framework.exceptions import APIException


class StockException(APIException):
    status_code = 404
    default_detail = "Symbol not Found"
    default_code = "Symbol_not_found"
