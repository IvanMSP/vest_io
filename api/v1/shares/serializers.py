# Django
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404

# Thirdy-Party
from rest_framework import serializers, status

# Owner
from .utils import NASDAQ
from shares.models import Stock, Share
from api.v1.vestio_framework.exceptions import StockException
from apps.reusable.choices import StatusType


class SellSerializer(serializers.Serializer):
    symbol = serializers.CharField(max_length=15, min_length=1)
    numberOfShares = serializers.IntegerField()

    def validate_symbol(self, value):
        stock = get_object_or_404(Stock, symbol=value)
        if not stock:
            raise StockException()
        return value

    def validate_numberOfShares(self, value):
        if value <= 0:
            raise serializers.ValidationError("Debe ser mayor a 0")
        return value

    def validate(self, data):
        user = self.context.get("user")
        number_of_shares = data["numberOfShares"]
        shares = user.shares.filter(
            stock__symbol=data["symbol"], status=StatusType.purchase.value
        ).count()
        if number_of_shares > shares:
            raise ValidationError("No tienes suficientes acciones para vender")
        return data


class BuySerializer(serializers.Serializer):
    symbol = serializers.CharField(max_length=15, min_length=1)
    numberOfShares = serializers.IntegerField()

    def validate_numberOfShares(self, value):
        """
        Check if the number of shares is greater than 0
        """
        if value <= 0:
            raise serializers.ValidationError("Debe ser mayor a 0")
        return value

    def save_stock(self):
        """
        Save Stock If not exists in DB or return instance Stock
        """
        symbol = self.data["symbol"]
        instance_nasdaq = NASDAQ()
        stock = instance_nasdaq.get_stock(symbol)
        if stock["status"]["rCode"] != 400:
            stock_instance, created = Stock.objects.get_or_create(
                symbol=stock["data"]["symbol"]
            )
            if created is True:
                stock_instance.symbol = stock["data"]["symbol"]
                stock_instance.company_name = stock["data"]["companyName"]
                stock_instance.save()
            return dict(
                instance=stock_instance, stock_data=stock["data"]["primaryData"]
            )
        raise StockException()


class ShareSerializer(serializers.ModelSerializer):
    class Meta:
        model = Share
        fields = "__all__"


class StockSerializer(serializers.ModelSerializer):
    shares = serializers.SerializerMethodField()
    companyName = serializers.CharField(source="company_name")
    totalShares = serializers.SerializerMethodField()
    profitLoss = serializers.SerializerMethodField()
    currentValueShares = serializers.SerializerMethodField()
    currentDayReferencePrices = serializers.SerializerMethodField()

    def get_totalShares(self, obj):
        user = self.context.get("user")
        return obj.shares.filter(user=user, status=StatusType.purchase.value).count()

    def get_profitLoss(self, obj):
        return 0

    def get_currentValueShares(self, obj):
        return 0

    def get_currentDayReferencePrices(self, obj):
        return 0

    def get_shares(self, obj):
        user = self.context.get("user")
        shares = obj.shares.filter(user=user, status=StatusType.purchase.value)
        return ShareSerializer(shares, many=True).data

    class Meta:
        model = Stock
        fields = [
            "symbol",
            "companyName",
            "totalShares",
            "profitLoss",
            "currentValueShares",
            "currentDayReferencePrices",
            "shares",
        ]
