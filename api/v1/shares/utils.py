# Python
import json
import requests
from requests.models import Response

# Owner
from apps.reusable.constants import NULL_VALUE


class NASDAQ:
    def __init__(self) -> None:
        self.url = "https://api.nasdaq.com/api/quote/"
        self.user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"

    def get_data(self, response: Response) -> dict:
        try:
            data = json.loads(response.text)
            return data
        except Exception as e:
            return str(e)

    def get_stock(self, symbol: str) -> dict:
        try:
            build_url = f"{self.url}{symbol}/info?assetclass=stocks"
            response = requests.get(build_url, headers={"User-Agent": self.user_agent})
            data = self.get_data(response)
            return data
        except requests.exceptions.InvalidHeader as e:
            return str(e)
        except requests.exceptions.Timeout as e:
            return str(e)
