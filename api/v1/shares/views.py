# Thirdy
from rest_framework import serializers, status
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

# Owner
from api.v1.vestio_framework.response import EnvelopeErrorResponse, EnvelopeResponse
from api.v1.vestio_framework.exceptions import StockException
from apps.reusable.choices import StatusType
from .serializers import BuySerializer, SellSerializer, StockSerializer
from shares.models import Share, Stock


class BuyShare(APIView):
    """
    View for buy shares of a stock

    * Requires Authorization Token
    * Requires symbol, number of shares to buy
    """

    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:
            data = request.data
            serializer = BuySerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                data_stock = serializer.save_stock()
                for _ in range(int(data["numberOfShares"])):
                    Share.objects.create(
                        stock=data_stock["instance"],
                        user=request.user,
                        last_sale=data_stock["stock_data"]["lastSalePrice"],
                        net_change=data_stock["stock_data"]["netChange"],
                        percentage_change=data_stock["stock_data"]["percentageChange"],
                        status=StatusType.purchase.value,
                    )
                return EnvelopeResponse(
                    "Compra exitosa", status=status.HTTP_201_CREATED
                )
        except ValidationError as exc:
            return EnvelopeErrorResponse(exc.detail, status=exc.status_code)
        except StockException as se:
            return EnvelopeErrorResponse(se.detail, status=se.status_code)
        except Exception as e:
            return EnvelopeErrorResponse(
                str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )


class SellShares(APIView):
    """
    View Sell Shares of a Stock
    * Requires Authorization Token
    * Requires symbol, number of shares to sell
    """

    permission_classes = [IsAuthenticated]

    def post(self, request):
        try:
            data = request.data
            user = request.user
            serializer = SellSerializer(data=data, context={"user": user})
            if serializer.is_valid(raise_exception=True):
                shares = user.shares.filter(
                    stock__symbol=data["symbol"], status=StatusType.purchase.value
                )[: int(data.get("numberOfShares"))]
                for share in shares:
                    share.status = StatusType.sale.value
                    share.save()
                return EnvelopeResponse("Venta Exitosa", status=status.HTTP_200_OK)
        except ValidationError as err:
            return EnvelopeErrorResponse(err.detail, status=err.status_code)


class Stocks(APIView):
    """
    View List Stocks of a user
    """

    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        stocks = (
            Stock.objects.prefetch_related("shares")
            .filter(shares__user=user, shares__status=StatusType.purchase.value)
            .distinct("symbol")
        )
        serializer = StockSerializer(stocks, context={"user": user}, many=True)
        return EnvelopeResponse(serializer.data, status=status.HTTP_200_OK)
