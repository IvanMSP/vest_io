# Django Core
from django.urls import path

# Owner
from .views import BuyShare, Stocks, SellShares

urlspatterns = [
    path("buy", BuyShare.as_view(), name="buy_share"),
    path("sell", SellShares.as_view(), name="sell_share"),
    path("stocks", Stocks.as_view(), name="stocks"),
]
