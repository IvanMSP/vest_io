# Django Core
from django.urls import path

# Owner
from .views import SignAuth


urlspatterns = [
    path("login", SignAuth.as_view(), name="login"),
]
