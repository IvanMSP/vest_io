# Django Core
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

# Thirdy Party
from rest_framework import status
from rest_framework.views import APIView

# Owner
from .serializers import UserBaseSerializer
from apps.reusable.constants import NULL_VALUE
from api.v1.vestio_framework.response import EnvelopeResponse, EnvelopeErrorResponse


class SignAuth(APIView):
    """
    Methods: POST
    View for register/login User
    """

    authentication_classes = []

    def post(self, request, *args, **kwargs):
        data = request.data
        username = data.get("username")
        password = data.get("password")
        email = data.get("email")
        if email in NULL_VALUE or username in NULL_VALUE or password in NULL_VALUE:
            return EnvelopeErrorResponse(
                "Fields are missing", status=status.HTTP_400_BAD_REQUEST
            )
        user, created = User.objects.get_or_create(username=username)
        if created is True:
            response_status = status.HTTP_201_CREATED
            user.email = email
            user.password = make_password(password)
        else:
            response_status = status.HTTP_200_OK
        serializer = UserBaseSerializer(user)
        user.save()
        return EnvelopeResponse(serializer.data, status=response_status)
