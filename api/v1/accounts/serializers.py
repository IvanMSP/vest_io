# django Core
from django.contrib.auth.models import User

# Thirdy-Party
from rest_framework import serializers
from rest_framework.authtoken.models import Token


class UserBaseSerializer(serializers.ModelSerializer):
    """
    Serializer User Basic Data
    endpoint: /login
    """

    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            "pk",
            "token",
            "email",
            "username",
        ]

    def get_token(self, user):
        token, created = Token.objects.get_or_create(user=user)
        if created is False:
            token.delete()
            token = Token.objects.create(user=user)
        return token.key
