# Django Core
from django.urls import include, path

# Owner
from .accounts.urls import urlspatterns as accounts_urls
from .shares.urls import urlspatterns as shares_urls

urlpatterns = [
    path("accounts/", include(accounts_urls)),
    path("shares/", include(shares_urls)),
]
