# Django Core
from django.contrib import admin
from django.urls import path
from django.urls.conf import include

# Urls
from api.v1.urls import urlpatterns as api_urls

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include(api_urls)),
]
