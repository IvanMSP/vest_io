import os
from .base import *
from decouple import config

SECRET_KEY = config("SECRET_KEY")
ALLOWED_HOSTS = ["*"]
DEBUG = config("DEBUG", default=True, cast=bool)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}
