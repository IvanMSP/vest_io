# OS
from enum import IntEnum


class StatusType(IntEnum):
    sale = 1
    purchase = 2

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]
