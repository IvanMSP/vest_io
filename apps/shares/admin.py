# Django
from django.contrib import admin

# Owner
from .models import Stock, Share


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ["symbol", "company_name"]


@admin.register(Share)
class ShareAdmin(admin.ModelAdmin):
    list_display = ["last_sale", "stock", "user", "status"]
    list_filter = ["user", "status"]
