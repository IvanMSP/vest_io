# Django
from django.db import models
from django.contrib.auth.models import User

# Reusable
from reusable.constants import BLANK, REQUIRED
from reusable.models import TimeStampModel
from reusable.choices import StatusType


class Stock(TimeStampModel):
    symbol = models.CharField(verbose_name="Simbolo", max_length=5, **REQUIRED)
    company_name = models.CharField(
        verbose_name="Nombre de la compañia", max_length=120, **REQUIRED
    )
    description = models.TextField(verbose_name="Descripción", **BLANK)

    class Meta:
        verbose_name = "Stock"
        verbose_name_plural = "Stocks"

    def __str__(self) -> str:
        return self.symbol


class Share(TimeStampModel):
    last_sale = models.CharField(
        default=0.0, max_length=120, verbose_name="Ultima venta", **REQUIRED
    )
    net_change = models.CharField(
        verbose_name="Cambio neto", max_length=120, **REQUIRED
    )
    percentage_change = models.CharField(
        verbose_name="Porcentaje de cambio", max_length=120, **REQUIRED
    )
    status = models.PositiveSmallIntegerField(
        verbose_name="Estatus", choices=StatusType.choices()
    )
    stock = models.ForeignKey(
        Stock,
        verbose_name="Stock",
        related_name="shares",
        on_delete=models.PROTECT,
        **REQUIRED,
    )
    user = models.ForeignKey(
        User,
        verbose_name="User",
        related_name="shares",
        on_delete=models.PROTECT,
        **REQUIRED,
    )

    class Meta:
        verbose_name = "Share"
        verbose_name_plural = "Shares"

    def __str__(self) -> str:
        return f"Share of {self.user} - {self.stock}"
