.PHONY: run migrate showmigrations

DOCKER_NAME=vest
POSTGRES_PASSWORD=TRES#$%@117
NAME_USER_DB=vest_db


# Colors for echos
ccend=$(shell tput sgr0)
ccbold=$(shell tput bold)
ccgreen=$(shell tput setaf 2)
ccso=$(shell tput smso)

VENV_NAME?=env
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python

.DEFAULT: help

help:
	@echo "$(ccgreen) #### ENVIROMENT COMMANDS#### $(ccend)"
	@echo "                                            "
	@echo "$(ccgreen)$(ccbold) make prepare-dev$(ccend)"
	@echo "       $(ccbold)make create-env$(ccend) "
	@echo "       $(ccbold)make install-deps$(ccend) "
	@echo "                                            "
	@echo "$(ccgreen) #### DJANGO COMMANDS#### $(ccend)"
	@echo "                                            "
	@echo "$(ccgreen)$(ccbold) make run$(ccend)        "
	@echo "       $(ccbold)manage.py runserver$(ccend) "
	@echo "$(ccgreen)$(ccbold) make migrations$(ccend)"
	@echo "       $(ccbold)manage.py makemigrations$(ccend)"
	@echo "$(ccgreen)$(ccbold) make migrate$(ccend)    "
	@echo "       $(ccbold)manage.py migrate$(ccend)   "
	@echo "$(ccgreen)$(ccbold) make showmigrations$(ccend)"
	@echo "       $(ccbold)manage.py showmigrations$(ccend)"
	@echo "$(ccgreen)$(ccbold) make createsuperuser$(ccend)"
	@echo "       $(ccbold)manage.py createsuperuser$(ccend)"
	@echo "                                            "
	@echo "$(ccgreen) #### RESTORE DATABASE COMMANDS#### $(ccend)"
	@echo "$(ccgreen)$(ccbold) make create-database$(ccend)      "
	@echo "       $(ccbold)download-image$(ccend) "
	@echo "       $(ccbold)create-container$(ccend)   "
	@echo "$(ccgreen)$(ccbold) make restore-database$(ccend)"
	@echo "       $(ccbold)psql -U $(NAME_USER_DB) < backup.sql$(ccend) "
	@echo "$(ccgreen)$(ccbold) make check-database$(ccend)"
	@echo "       $(ccbold)psql -U $(NAME_USER_DB)"
	@echo "													"
	@echo "$(ccgreen) #### DOCKER BASIC COMMANDS#### $(ccend)"
	@echo "$(ccgreen)$(ccbold) make start-container$(ccend)      "
	@echo "$(ccgreen)$(ccbold) make stop-container$(ccend)      "
	@echo "$(ccgreen)$(ccbold) make rm-container$(ccend)      "


venv: $(VENV_NAME)/bin/activate

# use only once
prepare-dev:
	make create-env
	make install-deps

create-env:
	virtualenv -p python3.9 $(VENV_NAME)

install-deps:venv
	${PYTHON} -m pip install -r requirements/develop.txt


# Django Commands

run:venv
	@echo "$(ccso) -->Runserver<-- $(ccend)"
	${PYTHON} manage.py runserver

migrate:venv
	@echo "$(ccbold) -->Migrate<-- $(ccgreen)"
	${PYTHON} manage.py migrate

showmigrations:venv
	@echo "$(ccgreen) -->Showmigrations<-- $(ccend)"
	${PYTHON} manage.py showmigrations

migrations:venv
	@echo "$(ccgreen)Makemigrations$(ccend)"
	${PYTHON} manage.py makemigrations

createsuperuser:
	@echo "$(ccgreen)Create super user$(ccend)"
	${PYTHON} manage.py createsuperuser

# Restore database Commands
create-database: download-image create-container

download-image:
	@echo "$(ccbold)---->Downloading image postgres$(ccend)"
	docker pull postgres:13

create-container:
	@echo "$(ccbold)---->Creando contenedor$(ccend)"
	docker run --name $(DOCKER_NAME) \
    -e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) \
    -e POSTGRES_USER=$(NAME_USER_DB) \
    -p 3345:5432 \
    -d \
    postgres:13

restore-database:
	@echo "$(ccbold)---->Restore DB$(ccend)"
	docker exec -i $(DOCKER_NAME) psql -U $(NAME_USER_DB) < backup.sql

check-database:
	@echo "$(ccbold)---->Check DB$(ccend)"
	docker exec -it $(DOCKER_NAME) psql -U $(NAME_USER_DB)


# Docker basic commands

start-container:
	@echo "$(ccbold)---->Starting container$(ccend)"
	docker container start $(DOCKER_NAME)

stop-container:
	@echo "$(ccbold)---->Stoping container$(ccend)"
	docker container stop $(DOCKER_NAME)

rm-container:
	@echo "$(ccbold)---->Removing container$(ccend)"
	docker container rm $(DOCKER_NAME)