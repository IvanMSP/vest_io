
# Vest io challenge


## Documentation

### Entity Realtional Diagram
![ERD](https://i.imgur.com/tLzfhBS.png)


### Process Buy Shares
![Process](https://i.imgur.com/4MdIHqF.png)


### Tech Stack

* Django 3.2.3
* Django Rest Framework
* Python 3.9
* Postgres 13.0
* Virtualenv
* Gitlab (Cliente de Git)

### Packages

* pre-commit = https://github.com/pre-commit/pre-commit
* Coverage = https://github.com/nedbat/coveragepy
* Black = https://github.com/psf/black
* Pytest = https://docs.pytest.org/en/stable/
* Psycopg2-binary = https://www.psycopg.org/
* Flake8 = https://github.com/PyCQA/flake8
* Pudb = https://pypi.org/project/pudb/
* Python-decouple = https://pypi.org/project/python-decouple/



### Configure Pre-commit

#### Install package

  ```bash
  pip install pre-commit==2.12.0
  ```

#### Add settings(.pre-commit-config.yaml)

  ```bash
  repos:
  -   repo: https://github.com/ambv/black
      rev: 20.8b1
      hooks:
      - id: black
        language_version: python3.9.1
  -   repo: https://gitlab.com/pycqa/flake8
      rev: 3.9.0
      hooks:
      - id: flake8
  -   repo: https://github.com/Lucas-C/pre-commit-hooks-safety
      rev: v1.2.1
      hooks:
      -   id: python-safety-dependencies-check
  ```

#### Install pre-commit owner repository

  ```bash
  pre-commit install
  ```

## Run locally

Clone project

```bash
  git clone https://gitlab.com/IvanMSP/vest_io.git
```

locate folder project

```bash
  cd vest_io
```

Open Makefile and follow commands for restore database
```bash
  make help
```

### Enviroment Variables
Add the following environment variables and value to file **.env**


`NAMEDB`

`USERDB`

`PASSWORDDB`

`HOST`

`PORTDB`

`SECRET_KEY`

`DEBUG`

`SETTINGS_MODULE`


## Endpoints API

[Documentation Endpoints](https://documenter.getpostman.com/view/11766693/U16jLQWr)

### Next Steps

-    Dockerizar project.

## Important Notes

- No se desarrollo el endpoint del historico de los precios, por cuestiones de tiempo, pero les platico como lo tenia pensado hacer.

    - **Diagrama Entidad Relación**

        ![ERD](https://i.imgur.com/wlEr1OT.png)

    - **Endpoint**

        {{url}}/stocks/historic/{{symbol}}

        * Method: GET
        * symbol: Simbolo del Stock


    - **Obtener información del Stock**

        Se plantea crear una tarea periodica con celery que se ejecute cada hora, para obtener los precios de los stocks

        ![Obtener data Stock](https://i.imgur.com/LXmzqS5.png)